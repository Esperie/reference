# lambda
f = lambda x, y: x + y
t = lambda x: x * float(5)/9 - 32

# map
fm = map(f, (1, 2, 3, 4), (3, 4, 5, 6))
tm = map(t, (140, 120, 60, 50))

# filter
seq = (0, 1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1, 0)
tf = filter(lambda x: x % 2, seq)

# reduce
tr = reduce(f, seq)
cr = reduce(lambda a, b: a if (a > b) else b, seq)