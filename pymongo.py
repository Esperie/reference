"""
# Aggregation
duplicates = db.transcripts.aggregate([
    {'$group': {
        '_id': {'sha': '$SHA256', 'date': '$Date', 'title': '$Title'},
        'uniqueIds': {'$addToSet': '$_id'},
        'count': {'$sum':1}
}},
    {'$match': {
        'count': {'$gt': 1}
    }}
])

# Updating
db[col].update({'_id': entry['_id']},
               {'$set': {'Title': article_title,
                         'Words': article_words,
                         'Date': article_date,
                         'Text': article},
                '$unset': {'Article_raw': True,
                           'Article_num': True}})

# Using multi
db[col].update({}, {'$unset': {'Article_num': True}},
                 multi=True)
"""